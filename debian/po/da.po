# Danish translation gnubg.
# Copyright (C) 2010 gnubg & nedenstående oversættere.
# This file is distributed under the same license as the gnubg package.
# Joe Hansen (joedalton2@yahoo.dk), 2010..
#
msgid ""
msgstr ""
"Project-Id-Version: gnubg\n"
"Report-Msgid-Bugs-To: rra@debian.org\n"
"POT-Creation-Date: 2006-12-03 16:38-0800\n"
"PO-Revision-Date: 2010-11-27 12:42+0000\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../gnubg.templates:1001
msgid "Build bearoff database?"
msgstr "Kompiler bearoff-database?"

#. Type: boolean
#. Description
#: ../gnubg.templates:1001
msgid ""
"For maximum strength, GNU Backgammon needs a two-sided bearoff database, "
"used to evaluate positions in the end-game.  This database takes up 6.6MB of "
"disk space and requires several minutes to generate on a reasonably fast "
"computer.  GNU Backgammon is fully playable without this database, but will "
"use weaker heuristics for the end of the game."
msgstr ""
"For maksimal styrke skal GNU Backgammon bruge en tosidet bearoff-database, "
"som bruges til at evaluere placeringer i slutspillet. Denne database fylder "
"6,6 MB og kræver flere minutter til oprettelsen på en rimelig hurtig "
"computer. GNU Backgammon kan spilles fuldt ud uden databasen, men vil bruge "
"en svagere heuristik i slutningen af spillet."
